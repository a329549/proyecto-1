const { Router } = require('express');
const express = require('express');
const router = express.Router();

const controller = require('../controllers/project')

router.get('/', controller.verProyectos);

router.get('/:id', controller.verProyecto)

router.post('/', controller.crearProyecto)

router.put('/:id', controller.editar)

router.patch('/:id', controller.actualizar)

router.delete('/:id', controller.destruir)

module.exports = router;