const express = require('express');
const router = express.Router();

const controller = require('../controllers/column')

// Paths
router.get('/', controller.verColumnas);

router.get('/:id', controller.verColumna)

router.post('/', controller.crearColumna)

router.put('/:id', controller.editarColumna)

router.patch('/:id', controller.actualizar)

router.delete('/:id', controller.destruir)

module.exports = router;