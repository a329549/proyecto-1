const { Router } = require('express');
const express = require('express');
const router = express.Router();

const controller = require('../controllers/members')

router.get('/', controller.verRegistros);

router.get('/:id', controller.verRegistro)

router.post('/', controller.crearUsuario)

router.put('/:id', controller.editarRegistro)

router.patch('/:id', controller.actualizar)

router.delete('/:id', controller.destruir)

module.exports = router;