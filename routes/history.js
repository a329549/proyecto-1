const { Router } = require('express');
const express = require('express');
const router = express.Router();

const controller = require('../controllers/history')

router.get('/', controller.verHistorias);

router.get('/:id', controller.verHistoria)

router.post('/', controller.crearHistoria)

router.put('/:id', controller.editarHistoria)

router.patch('/:id', controller.actualizar)

router.delete('/:id', controller.destruir)

module.exports = router;
