# Diagrama de clases
![](./image.png)

## Instrucciones
Para correr el proyecto primero hay que instalar las dependencias de node con el comando

```bash
npm install
```

Si queremos correr el proyecto únicamente con node, ejecutamos el siguiente comando

```bash
DEBUG=proyecto-1:* npm start
```

Después de ejecutar esta línea, nos montará el proyecto en el puerto 3000.

Pero si queremos utilizar docker para correr el proyecto, utilizamos este comando

```bash
docker-compose up -d
```
La bandera -d es para no ejecutar el proceso en el hilo principal de la terminal. Podemos
verificar que estén corriendo las imágenes con el comando

```bash
docker-compose ps
```

Para terminar la ejecución de ambas imágenes utilizamos

```bash
docker-compose down
```


## Equipo 📌
* ALEJANDRO ISAAC BACA SIERRA (320769)
* DANIEL LOPEZ VILLALOBOS (315120)
* EDWIN ALFREDO PINEDO CHACON (329549)
* JORGE AARON HERNANDEZ BUSTILLOS (329747)