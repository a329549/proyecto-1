const express = require('express')

function verHistorias(req, res, next) {
    res.send('Lista de historias');
}
function verHistoria(req, res, next){
    res.send(`Historia con id =  ${req.params.id}`)
}
function crearHistoria(req, res, next){
    const name = req.body.name;
    const role = req.body.rol;
    const fun = req.body.fun;
    const benefit = req.body.benefit;
    const priority = req.body.priority;
    const size = req.body.size;
    const context = req.body.context;
    const when = req.body.when;
    const results = req.body.results;
    res.send(`Crear nueva historia ${name} rol:${role},
            La funcionalidad sera: ${fun}, El beneficio: ${benefit}, Prioridad: ${priority}, Tamano: ${size}, Contexto: ${context}, Cuando ${when}, Resultados: ${results}`)
}
function editarHistoria(req, res, next){
    res. send(`Editar historia con id = ${req.params.id}`)
}
function actualizar(req, res, next){
    res. send(`Actualizar historia con id = ${req.params.id}`)
}
function destruir(req, res, next){
    res. send(`Eliminar historia con id = ${req.params.id}`)
}

module.exports = {
    verHistorias,
    verHistoria,
    crearHistoria,
    editarHistoria,
    actualizar,
    destruir
}
