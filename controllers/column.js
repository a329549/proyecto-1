const express = require('express')

//restfull => get, post, put, patch, delete
//modelo = estructura de datos que representa una entidad de mundo real

const verColumnas = (req,res,next) => {
    res.send('Lista con todas las columnas');
}

const verColumna = (req,res,next) => {
    res.send(`Mostar columna ${req.body.id}`);
    /*
        Mostrar información de la columna
    */
}

const crearColumna = (req,res,next) => {
    const sprintBacklog = req.body.sprint;
    res.send(`Se ha creado una nueva columna`);
}

const editarColumna = (req,res,next) => {
    res.send(`Columna ${req.body.id} editada.`);
}

const actualizar = (req,res,next) => {
    res.send(`Columna ${req.body.id} actualizada.`);
}

const destruir = (req,res,next) => {
    res.send(`Columna ${req.body.id} eliminada.`);
}

module.exports = {
    verColumnas,
    verColumna,
    crearColumna,
    editarColumna,
    actualizar,
    destruir
}