const express = require('express')

function verRegistros(req, res, next) {
    res.send('Lista de miembros registrados');
}
function verRegistro(req, res, next){
    res.send(`Miembro con id =  ${req.params.id}`)
}
function crearUsuario(req, res, next){
    const name = req.body.name;
    const lastName = req.body.lastName;
    const birthDay = req.body.birthDay;
    const curp = req.body.curp;
    const rfc = req.body.rfc;
    const address = req.body.address;
    const abilities = req.body.abilities;
    const team = req.body.team;
    res.send(`Crear miembro nuevo ${name} ${lastName},
            nacimiento: ${birthDay}, curp ${curp}, rfc ${rfc}, dirección ${address}, habilidades ${abilities}, equipo ${team}`)
}
function editarRegistro(req, res, next){
    res. send(`Editar miembro con id = ${req.params.id}`)
}
function actualizar(req, res, next){
    res. send(`Actualizar miembro con id = ${req.params.id}`)
}
function destruir(req, res, next){
    res. send(`Eliminar miembro con id = ${req.params.id}`)
}

module.exports = {
    verRegistros, 
    verRegistro, 
    crearUsuario, 
    editarRegistro, 
    actualizar, 
    destruir
}