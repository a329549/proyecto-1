const express = require('express')

function home(req, res, next) {
    res.render('index', { title: 'Manejador de Proyectos' });
}

module.exports = {
    home
}