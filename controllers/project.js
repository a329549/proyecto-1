const express = require('express')

function verProyectos(req, res, next) {
    res.send('Proyectos');
}
function verProyecto(req, res, next){
    res.send(`proyecto no. ${req.params.id}`)
}
function crearProyecto(req, res, next){
    const name = req.body.name;
    const date = req.body.date;
    const startDate = req.body.startDate;
    const description = req.body.description;
    const pManager = req.body.pManager;
    const prOwner = req.body.prOwner;
    const team = req.body.team;
    res.send(`Crear proyecto nuevo llamado ${name},
            fecha: ${date}, fecha de inicio ${startDate}, descripción: ${description}, project manager: ${pManager}, project owner: ${prOwner}, equipo: ${team}`)
}
function editar(req, res, next){
    res. send(`Editar proyecto no. ${req.params.id}`)
}
function actualizar(req, res, next){
    res. send(`Actualizar proyecto no. ${req.params.id}`)
}
function destruir(req, res, next){
    res. send(`Eliminar proyecto no. ${req.params.id}`)
}

module.exports = {
    verProyectos, 
    verProyecto, 
    crearProyecto, 
    editar, 
    actualizar, 
    destruir
}